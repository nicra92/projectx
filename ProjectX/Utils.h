#pragma once

#include "glew\glew.h"

bool ReadTxtFile(const char* fileName, std::string& outFile);
GLuint CreateVBO();
GLuint CreateIBO();