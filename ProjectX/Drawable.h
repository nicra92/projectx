#pragma once

#include "Math.h"
#include "glew\glew.h"
#include "Texture.h"
#include "Utils.h"

class Drawable {
public:
	Texture t;
	GLuint texNum;
	GLuint VBO, IBO;
	std::string textureFileName;
	Matrix3f ScaleTrans, RotateTrans, TranslationTrans;


	float posX = 0, posY = 0;

	void setPos(float x, float y) {
		posX = x; posY = y;

		TranslationTrans.InitTranslationTransform(posX, posY);
	}
	void moveBy(float x, float y) {
		posX += x; posY += y;

		TranslationTrans.InitTranslationTransform(posX, posY);
	}

	Drawable(std::string txtFN) {
		t.Load(GL_TEXTURE_2D, txtFN);
		VBO = CreateVBO();
		IBO = CreateIBO();
		texNum = t.m_textureObj;

		TranslationTrans.InitTranslationTransform(posX, posY);
	}

	Drawable(GLuint texNum) {
		this->texNum = texNum;
		VBO = CreateVBO();
		IBO = CreateIBO();

		TranslationTrans.InitTranslationTransform(posX, posY);
	}
	Drawable() {
		VBO = CreateVBO();
		IBO = CreateIBO();

		TranslationTrans.InitTranslationTransform(posX, posY);
	}

	virtual void render() {
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		t.Bind(GL_TEXTURE0 + texNum);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
	}

};