#pragma once

#include <stdio.h>
#include <string.h>

#ifndef M_PI
#define M_PI    3.14159265358979323846264338327950288   /**< pi */
#endif
#define ToRadian(x) (float)(((x) * M_PI / 180.0f))
#define ToDegree(x) (float)(((x) * 180.0f / M_PI))
struct Vector2i
{
	int x;
	int y;
};

struct Vector2f
{
	float x;
	float y;

	Vector2f()
	{
	}

	Vector2f(float _x, float _y)
	{
		x = _x;
		y = _y;
	}
};



struct Vector3f
{
	float x;
	float y;
	float z;

	Vector3f() {}

	Vector3f(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}

	Vector3f(const float* pFloat)
	{
		x = pFloat[0];
		y = pFloat[0];
		z = pFloat[0];
	}

	Vector3f(float f)
	{
		x = y = z = f;
	}

	Vector3f& operator+=(const Vector3f& r)
	{
		x += r.x;
		y += r.y;
		z += r.z;

		return *this;
	}

	Vector3f& operator-=(const Vector3f& r)
	{
		x -= r.x;
		y -= r.y;
		z -= r.z;

		return *this;
	}

	Vector3f& operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;

		return *this;
	}

	operator const float*() const
	{
		return &(x);
	}


	Vector3f Cross(const Vector3f& v) const;

	Vector3f& Normalize();

	void Rotate(float Angle, const Vector3f& Axis);

	void Print() const
	{
		printf("(%.02f, %.02f, %.02f)", x, y, z);
	}
};
struct Vertex
{
	Vector3f m_pos;
	Vector2f m_tex;

	Vertex() {}

	Vertex(Vector3f pos, Vector2f tex)
	{
		m_pos = pos;
		m_tex = tex;
	}
};

struct Quaternion
{
	float x, y, z, w;

	Quaternion(float _x, float _y, float _z, float _w);

	void Normalize();

	Quaternion Conjugate();

	Vector3f ToDegrees();
};

struct OrthoProjInfo
{
	float r;        // right
	float l;        // left
	float b;        // bottom
	float t;        // top
};

class Matrix3f
{
public:
	float m[3][3];

	Matrix3f()
	{
	}

	// constructor from Assimp matrix

	Matrix3f(float a00, float a01, float a02,
		float a10, float a11, float a12,
		float a20, float a21, float a22
		)
	{
		m[0][0] = a00; m[0][1] = a01; m[0][2] = a02;
		m[1][0] = a10; m[1][1] = a11; m[1][2] = a12;
		m[2][0] = a20; m[2][1] = a21; m[2][2] = a22;
	}

	void SetZero()
	{
		memset(m, 0, sizeof(m));
	}

	Matrix3f Transpose() const
	{
		Matrix3f n;

		for (unsigned int i = 0; i < 3; i++) {
			for (unsigned int j = 0; j < 3; j++) {
				n.m[i][j] = m[j][i];
			}
		}

		return n;
	}


	inline void InitIdentity()
	{
		m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f;
		m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f;
		m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f;
	}

	inline Matrix3f operator*(const Matrix3f& Right) const
	{
		Matrix3f Ret;

		for (unsigned int i = 0; i < 3; i++) {
			for (unsigned int j = 0; j < 3; j++) {
				Ret.m[i][j] = m[i][0] * Right.m[0][j] +
					m[i][1] * Right.m[1][j] +
					m[i][2] * Right.m[2][j];
			}
		}

		return Ret;
	}

	Vector3f operator*(const Vector3f& v) const
	{
		Vector3f r;

		r.x = m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z;
		r.y = m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z;
		r.z = m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z;

		return r;
	}

	operator const float*() const
	{
		return &(m[0][0]);
	}

	void Print() const
	{
		for (int i = 0; i < 3; i++) {
			printf("%f %f %f\n", m[i][0], m[i][1], m[i][2]);
		}
	}

	float Determinant() const;

	Matrix3f& Inverse();

	void InitScaleTransform(float ScaleX, float ScaleY);
	void InitRotateTransform(float Rotate);
	void InitRotateTransform(const Quaternion& quat);
	void InitTranslationTransform(float x, float y);
	void InitCameraTransform(const Vector3f& Target, const Vector3f& Up);
	//void InitPersProjTransform(const PersProjInfo& p);
	void InitOrthoProjTransform(const OrthoProjInfo& p);
};