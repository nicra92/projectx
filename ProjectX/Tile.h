#pragma once

#include "Drawable.h"
#include "TexMagazine.h"

enum TileType {
	EMPTY,
	WALL,
	FLOOR,
	COUNT
};

class Tile : public Drawable{
public:
	TileType type = TileType::EMPTY;

	int checkID = 0;

	Tile(const char* type) : Drawable(TexMagazine::textures.at(type)) {

	}
	Tile() : Drawable() {

	}

	void setAs(TileType newType) {
		type = newType;
		switch (newType)
		{
		case EMPTY:
			texNum = TexMagazine::get("emp");
			break;
		case WALL:
			texNum = TexMagazine::get("wal");
			break;
		case FLOOR:
			texNum = TexMagazine::get("flr");
			break;
		case COUNT:
			texNum = TexMagazine::get("emp");
			break;
		default:
			break;
		}
	}
};