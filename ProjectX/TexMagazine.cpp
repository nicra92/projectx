#include "TexMagazine.h"

TexMagazine::qMap TexMagazine::textures;
TexMagazine::wMap TexMagazine::texturesObj;
Texture TexMagazine::empty;
Texture TexMagazine::mark;
Texture TexMagazine::wall;
Texture TexMagazine::floor;

void TexMagazine::init(){
	empty.Load(GL_TEXTURE_2D, "img/emptyTile.png");
	textures.insert(qMap::value_type("emp", empty.m_textureObj));
	texturesObj.insert(wMap::value_type("emp", &empty));

	mark.Load(GL_TEXTURE_2D, "img/mark.png");
	textures.insert(qMap::value_type("mrk", mark.m_textureObj));
	texturesObj.insert(wMap::value_type("mrk", &mark));

	wall.Load(GL_TEXTURE_2D, "img/wall.png");
	textures.insert(qMap::value_type("wal", wall.m_textureObj));
	texturesObj.insert(wMap::value_type("wal", &wall));

	floor.Load(GL_TEXTURE_2D, "img/floor.png");
	textures.insert(qMap::value_type("flr", floor.m_textureObj));
	texturesObj.insert(wMap::value_type("flr", &floor));

}
