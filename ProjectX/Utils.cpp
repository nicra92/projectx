#include <string>
#include <fstream>
#include <Windows.h>

#include "Math.h"
#include "Utils.h"

bool ReadTxtFile(const char* pFileName, std::string& outFile)
{
	std::ifstream f(pFileName);

	bool ret = false;

	if (f.is_open()) {
		std::string line;
		while (getline(f, line)) {
			outFile.append(line);
			outFile.append("\n");
		}

		f.close();

		ret = true;
	}
	else {
		char buf[2048];
		sprintf_s(buf, "Cannot open file %s\n", pFileName);
		MessageBox(NULL, buf, "Error", MB_ICONERROR);
		exit(1);
	}

	return ret;
}

GLuint CreateVBO()
{
	GLuint VBO;
	Vertex Vertices[4] = { 
		Vertex(Vector3f(-0.05f, -0.05f, 1.f),	Vector2f(0.0f, 0.0f)),
		Vertex(Vector3f( 0.05f, -0.05f, 1.f),	Vector2f(1.0f, 0.0f)),
		Vertex(Vector3f( 0.05f,  0.05f, 1.f),	Vector2f(1.0f, 1.0f)),
		Vertex(Vector3f(-0.05f,  0.05f, 1.f),	Vector2f(0.0f, 1.0f)) };

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

	return VBO;
}

GLuint CreateIBO()
{
	GLuint IBO;

	unsigned int Indices[] = {	0, 1, 2,
								0,2,3};

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

	return IBO;
}