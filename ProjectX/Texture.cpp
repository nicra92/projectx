#include <string>
#include <Windows.h>

#include "SDL\SDL_image.h"
#include "glew\glew.h"
#include "Texture.h"

bool Texture::Load(GLenum TextureTarget, const std::string& FileName)
{
	m_textureTarget = TextureTarget;
	m_fileName = FileName;

	auto surf = IMG_Load(m_fileName.c_str());
	if (surf == NULL) {
		char buf[2048];
		sprintf_s(buf, "Cannot load file %s", m_fileName.c_str());
		MessageBox(NULL, buf, "Error", MB_ICONERROR);
		exit(1);
	}

	glGenTextures(1, &m_textureObj);
	glBindTexture(m_textureTarget, m_textureObj);

	int Mode = GL_RGB;

	if (surf->format->BytesPerPixel == 4) {
		Mode = GL_RGBA;
	}
	glTexImage2D(m_textureTarget, 0, Mode, surf->w, surf->h, 0, Mode, GL_UNSIGNED_BYTE, surf->pixels);

	glTexParameteri(m_textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(m_textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return true;
}

void Texture::Bind(GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(m_textureTarget, m_textureObj);
}