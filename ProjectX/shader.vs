#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 TexCoord;

uniform mat3 gWVP;
out vec2 TexCoord0;

void main()
{
    gl_Position = vec4(gWVP * Position, 1);
	//gl_Position = vec4(gWVP[0][2]+Position.x, gWVP[2][1]+Position.y,1,1);
    TexCoord0 = TexCoord;
}
