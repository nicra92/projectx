#pragma once

#include <string>


class Texture
{
public:
	bool Load(GLenum TextureTarget, const std::string& FileName);

	void Bind(GLenum TextureUnit);

	std::string m_fileName;
	GLenum m_textureTarget;
	GLuint m_textureObj;
};