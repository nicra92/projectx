#include <string>
#include <iostream>

#define GL3_PROTOTYPES 1

#include "glew/glew.h"
#include "SDL/SDL.h"
#include "Graphics.h"
#include "Player.h"
#include "ShipDesigner.h"

void RunGame(Graphics* mainGraph);

int main(int argc, char *argv[])
{
	Graphics mainGraphics;
	ShipDesigner sp;

	while (true) {
		if (sp.renderFrame(&mainGraphics) == -1) break;
	}
	//RunGame(&mainGraphics);

	mainGraphics.CleanUp();

	return 0;
}

void RunGame(Graphics* mainGraph)
{
	Player p;
	bool loop = true;

	while (loop)
	{
		mainGraph->Clear();

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				loop = false;

			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					loop = false;
					break;
				case SDLK_w:
					mainGraph->cam.move(0, 0.01f);
					break;
				case SDLK_s:
					mainGraph->cam.move(0, -0.01f);
					break;
				case SDLK_a:
					mainGraph->cam.move(-0.01f, 0.f);
					break;
				case SDLK_d:
					mainGraph->cam.move(0.01f, 0.f);
					break;
				case SDLK_r:
					mainGraph->cam.scaleMul(1.01f);
					break;
				case SDLK_f:
					mainGraph->cam.scaleMul(0.99f);
					break;
				default:
					break;
				}
			}

		}
		mainGraph->render(&p);
		mainGraph->Swap();
	}
}

