#pragma once
#include "SDL\SDL.h"
#include "Camera.h"
#include "Drawable.h"

class Graphics {
public:


	SDL_Window *mainWindow;

	SDL_GLContext mainContext;

	int scrX = 1000;
	int scrY = 700;

	float ppu = 0;

	Camera cam;

	bool fullscreen = false;

	Graphics();
	void setupOGL();
	void CleanUp();

	void Clear();
	void Swap();

	void render(Drawable *d);

	void mousePositionToCoords(int x, int y, float *rx, float *ry);

	GLuint shaderProgram = 0;
	GLuint samplerLocation = 0;
	GLuint gWVPLocation = 0;
};