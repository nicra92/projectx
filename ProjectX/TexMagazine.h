#pragma once
#include <map>
#include "glew\glew.h"
#include "Texture.h"


class TexMagazine {
public:

	struct char_cmp {
		bool operator () (const char *a, const char *b) const
		{
			return strcmp(a, b)<0;
		}
	};
	typedef std::map<const char *, int, char_cmp> qMap;
	typedef std::map<const char *, Texture*, char_cmp> wMap;

	static qMap textures;
	static wMap texturesObj;

	static Texture empty;
	static Texture mark;
	static Texture wall;
	static Texture floor;

	static void init();
	
	static GLuint get(const char* id) {
		return textures.at(id);

	}


};