#include "Graphics.h"


#include <Windows.h>
#include "ShaderLoad.h"
#include "glew\glew.h"
#include "SDL\SDL.h"
#include "TexMagazine.h"

char* projectName = "ProjectX v0.01";

Graphics::Graphics() {
	if (SDL_Init(SDL_INIT_VIDEO) > 0)
	{
		MessageBox(NULL, "Failed to init SDL", "Error", MB_ICONERROR);
		exit(0);
	}

	mainWindow = SDL_CreateWindow(projectName, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		scrX, scrY, SDL_WINDOW_OPENGL);

	if (!mainWindow)
	{
		MessageBox(NULL, "Unable to create window", "Error", MB_ICONERROR);
		exit(0);
	}

	mainContext = SDL_GL_CreateContext(mainWindow);

	setupOGL();

	SDL_GL_SetSwapInterval(1);

	glewExperimental = GL_TRUE;
	glewInit();

	shaderProgram = CompileShaders("shader.vs", "shader.fs");
		
	samplerLocation = glGetUniformLocation(shaderProgram, "gSampler");
	glUniform1i(samplerLocation, 0);

	gWVPLocation = glGetUniformLocation(shaderProgram, "gWVP");

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(mainWindow);

	float ratio = (float)scrX / (float)scrY;
	ppu = (float)scrY / 2.f;
	cam.setProj(ratio*2, 2);
	TexMagazine::init();
}

void Graphics::mousePositionToCoords(int x, int y, float *rx, float *ry) {

	*rx = x - (scrX / 2);
	*ry = -(y - (scrY / 2));
	*rx /= ppu * cam.scal;
	*ry /= ppu * cam.scal;
	*rx -= cam.cx;
	*ry -= cam.cy;
}

void Graphics::render(Drawable *obj) {
	glUniformMatrix3fv(gWVPLocation, 1, GL_TRUE, (const GLfloat*)((cam.res * obj->TranslationTrans).m));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, obj->VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, obj->IBO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, obj->texNum);
	//obj->t.Bind(GL_TEXTURE0 + obj->texNum);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void Graphics::setupOGL() {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Graphics::CleanUp() {
	SDL_GL_DeleteContext(mainContext);

	SDL_DestroyWindow(mainWindow);

	SDL_Quit();
}

void Graphics::Clear() {
	glClear(GL_COLOR_BUFFER_BIT);
}

void Graphics::Swap() {
	SDL_GL_SwapWindow(mainWindow);
}