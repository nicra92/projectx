#pragma once

#include <vector>

#include "Tile.h"
#include "Graphics.h"

#define MAX_X 40
#define MAX_Y 40

class ShipDesigner {
public:
	typedef std::vector<Tile*> Row;
	typedef std::vector<Row> Grid;

	int mX = 0, mY = 0;
	int sX, sY;

	Grid grid;

	Tile *selected = NULL;
	Drawable mark;

	ShipDesigner() {
		grid = Grid(MAX_X, Row(MAX_Y));
		mark.texNum = TexMagazine::get("mrk");
		for (int i = 0; i < MAX_X; i++)
		{
			for (int j = 0; j < MAX_Y; j++)
			{
				grid[i][j] = new Tile("emp");
				grid[i][j]->setPos((i - (MAX_X/2))*0.1f, (j - (MAX_Y/2)) * 0.1f);
			}
		}
	}

	~ShipDesigner() {

		for (size_t i = 0; i < MAX_X; i++)
		{
			for (size_t j = 0; j < MAX_Y; j++)
			{
				delete grid[i][j];
			}
		}
	}

	int checkID = 0;

	int floorCheck(int sX, int sY, bool fill, bool start = true, int maxW = 0) {
		//if (maxW >= 1000) return -1;
		if (start) checkID++;
		if (sX < 0 || sY < 0 || sX >= MAX_X || sY >= MAX_Y) return -1;
		if (grid[sX][sY]->checkID == checkID) return 0;
		if (!fill && grid[sX][sY]->type == TileType::FLOOR) {
		}
		else if (grid[sX][sY]->type != TileType::EMPTY) return 1;

		grid[sX][sY]->checkID = checkID;
		int a = (floorCheck(sX - 1, sY, fill, false, maxW+1));
		int b = (floorCheck(sX + 1, sY, fill, false, maxW + 1));
		int c = (floorCheck(sX, sY + 1, fill, false, maxW + 1));
		int d = (floorCheck(sX, sY - 1, fill, false, maxW + 1));
		if ((!fill) && (a == -1 || b == -1 || c == -1 || d == -1)) {
			if (start) {
				for (size_t i = 0; i < MAX_X; i++)
				{
					for (size_t j = 0; j < MAX_Y; j++)
					{
						if (grid[i][j]->checkID == checkID) {
							grid[i][j]->setAs(TileType::EMPTY);
						}
					}
				}
			}
			return -1;
		}
		else if((!fill) && start) {
			grid[sX][sY]->setAs(TileType::FLOOR);
		}

		if(fill && (a == -1 || b == -1 || c == -1 || d == -1)) return -1;
		else if(fill && start) {
			for (size_t i = 0; i < MAX_X; i++)
			{
				for (size_t j = 0; j < MAX_Y; j++)
				{
					if (grid[i][j]->checkID == checkID) {
						grid[i][j]->setAs(TileType::FLOOR);
					}
				}
			}
		}
	}

	int renderFrame(Graphics* g) {
		g->Clear();

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				return -1;
			if (event.type == SDL_MOUSEMOTION) {
				mX = event.motion.x;
				mY = event.motion.y;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN) {
				if (event.button.button == SDL_BUTTON_LEFT) {
					if (selected) {
						selected->setAs(TileType::WALL);
						floorCheck(sX - 1, sY, true);
						floorCheck(sX + 1, sY, true);
						floorCheck(sX, sY + 1, true);
						floorCheck(sX, sY - 1, true);
					}
				}
				if (event.button.button == SDL_BUTTON_RIGHT) {
					if (selected && selected->type != TileType::EMPTY && selected->type != TileType::FLOOR) {
						
						selected->setAs(TileType::EMPTY);
						floorCheck(sX, sY, false);
						//floorCheck(sX + 1, sY, false);
						//floorCheck(sX, sY + 1, false);
						//floorCheck(sX, sY - 1, false);
					}
				}
			}
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					return -1;
					break;
				case SDLK_w:
					g->cam.move(0, -0.01f);
					break;
				case SDLK_s:
					g->cam.move(0, 0.01f);
					break;
				case SDLK_a:
					g->cam.move(0.01f, 0.f);
					break;
				case SDLK_d:
					g->cam.move(-0.01f, 0.f);
					break;
				case SDLK_r:
					g->cam.scaleMul(1.01f);
					break;
				case SDLK_f:
					g->cam.scaleMul(0.99f);
					break;
				default:
					break;
				}
			}

		}

		float x, y;
		g->mousePositionToCoords(mX, mY, &x, &y);
		
		sX = (int)(x*10.f + (MAX_X / 2) + 0.5f);
		sY = (int)(y*10.0f + (MAX_Y / 2) + 0.5f);
		std::cout << (int)(x*10.f + (MAX_X / 2) +0.5f) << " " << (int)(y*10.0f +(MAX_Y/2) +0.5f) << "\n";


		

		for (size_t i = 0; i < MAX_X; i++)
		{
			for (size_t j = 0; j < MAX_Y; j++)
			{
				g->render(grid[i][j]);
			}
		}
		if (sX >= 0 && sY >= 0 && sX < MAX_X && sY < MAX_Y) {
			selected = grid[sX][sY];
			mark.setPos(selected->posX, selected->posY);
			g->render(&mark);
		}
		else
			selected = NULL;

		g->Swap();
	}


};