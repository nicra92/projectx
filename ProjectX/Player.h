#pragma once

#include "Math.h"
#include "glew\glew.h"
#include "Texture.h"
#include "Utils.h"
#include "Drawable.h"

class Player : public Drawable{
public:
	Texture t;
	GLuint VBO, IBO;

	Player() : Drawable("img/pl.png"){
	}
};