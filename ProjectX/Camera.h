#pragma once

#include "Math.h"

class Camera {
public:
	Matrix3f CamScaleTrans, CamRotateTrans, CamTranslationTrans, Proj;

	Matrix3f res;
	float cx = 0, cy = 0, rot = 0, scal = 1;

	Camera() {
		CamTranslationTrans.InitTranslationTransform(cx, cy);
		CamRotateTrans.InitRotateTransform(rot);
		CamScaleTrans.InitScaleTransform(scal, scal);

		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}

	void setProj(float x, float y) {
		OrthoProjInfo opi;
		opi.l = -x / 2.f;
		opi.r = x / 2.f;
		opi.b = -y / 2.f;
		opi.t = y / 2.f;
		Proj.InitOrthoProjTransform(opi);
		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}

	void move(float dx, float dy) {
		cx += dx; cy += dy;
		CamTranslationTrans.InitTranslationTransform(cx, cy);
		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}

	void rotate(float dRot) {
		rot += dRot;
		CamRotateTrans.InitRotateTransform(rot);
		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}
	
	void setRotate(float dRot) {
		rot = dRot;
		CamRotateTrans.InitRotateTransform(rot);
		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}

	void scaleAdd(float scale) {
		scal += scale;
		CamScaleTrans.InitScaleTransform(scal, scal);
		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}

	void scaleMul(float scale) {
		scal *= scale;
		CamScaleTrans.InitScaleTransform(scal, scal);
		res = Proj * CamScaleTrans * CamRotateTrans * CamTranslationTrans;
	}
};